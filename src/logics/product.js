import { helpers } from '@/utils/helpers.js'
import { $HTTP } from '@/utils/axios'

const getProductList = async (Bale) => {
  try {
    helpers.showLoader('Getting data...')

    let res = await $HTTP.get(`/product/list?Page=1&Size=100&CultureCode=EN`)
    helpers.hideLoader()
    if (res && res.status === 200 && res.data && res.data.data) {
      return res.data.data
    } else {
      throw new Error('Cannot get product lists, Please try again later.')
    }
  } catch (err) {
    helpers.hideLoader()
    helpers.catchError(err, true)
  }
}



export {getProductList}
