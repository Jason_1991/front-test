import axios from 'axios'
import { configs } from '@/utils/configs'

const HTTP = axios.create({
  baseURL: configs.baseURL,
  timeout: 30000
})

HTTP.interceptors.request.use((config) => {
  // const userToken =
  //   window.sessionStorage.getItem(configs.tokenSession) ||
  //   window.localStorage.getItem(configs.tokenSession)

  // if (userToken && userToken !== '') {
  //   config.headers.Authorization = `Bearer ${userToken}`
  // }

  return config
})

const install = (app) => {
  app.config.globalProperties.$HTTP = HTTP
}

export { install as default, HTTP as $HTTP }
