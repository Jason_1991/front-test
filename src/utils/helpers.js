import { configs } from '@/utils/configs'
import { f7 } from 'framework7-vue'

import dayjs from 'dayjs'

const helpers = {
  isEmailValid(email){
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  },
  cutText(text, length) {
    if (text.split(' ').length > 1) {
      const string = text.substring(0, length)
      const splitText = string.split(' ')
      splitText.pop()
      return splitText.join(' ') + '...'
    } else {
      return text
    }
  },
  capitalizeFirstLetter(string) {
    if (string) {
      return string.charAt(0).toUpperCase() + string.slice(1)
    } else {
      return ''
    }
  },
  padStart(value, numberOfPad, padChar) {
    let pattern = ''
    for (let i = 0; i < numberOfPad; i++) {
      pattern += padChar
    }

    return (pattern + value).slice(-numberOfPad)
  },
  formatPHMobile({ MobileCode = '+63', MobileNumber }) {
    return `${MobileCode} ${MobileNumber.substring(0, 3)} ${MobileNumber.substring(3, 7)} ${MobileNumber.substring(7, 10)}`
  },
  formatDate(date) {
    let isValidDate = dayjs(date).isValid()
    if (isValidDate) {
      return dayjs(date).format('MMM D, YYYY')
    } else {
      return '--'
    }
  },
  formatDateTime(date) {
    let isValidDate = dayjs(date).isValid()
    if (isValidDate) {
      return dayjs(date).format('MMM D, YYYY h:mm A')
    } else {
      return '--'
    }
  },
  formatPascalCase(str) {
    return (
      str
        // Look for long acronyms and filter out the last letter
        .replace(/([A-Z]+)([A-Z][a-z])/g, ' $1 $2')
        // Look for lower-case letters followed by upper-case letters
        .replace(/([a-z\d])([A-Z])/g, '$1 $2')
        // Look for lower-case letters followed by numbers
        .replace(/([a-zA-Z])(\d)/g, '$1 $2')
        .replace(/^./, function(str) {
          return str.toUpperCase()
        })
        // Remove any white space left around the word
        .trim()
    )
  },
  formatConfigValue(str) {
    return helpers.formatPascalCase(str.replace(/^.+\./, ''))
  },
  onlyNumber(string) {
    if (string) {
      return string.replace(/\D/g, '')
    } else {
      return ''
    }
  },
  isset(obj) {
    if (obj !== null && obj !== undefined) {
      if (typeof obj === 'object' || Array.isArray(obj)) {
        return Object.keys(obj).length
      } else {
        return obj.toString().length
      }
    }

    return false
  },
  isBlank: (value) => {
    if (value === null || value === undefined || value === '' || value.length === 0) {
      return true
    } else {
      return false
    }
  },
  isValidTableId: (val) => {
    return val && parseInt(val) > 0
  },
  isUndefined: (val) => {
    return typeof val === 'undefined'
  },
  isInteger: (val) => {
    return helpers.isUndefined(val) ? false : Number.isInteger(!isNaN(val) ? Number(val) : null)
  },
  isString: (val) => {
    return typeof val === 'string' || val instanceof String
  },
  isObject: (val) => {
    return !helpers.isUndefined(val) && val.toString() === '[object Object]'
  },
  isArray: (val) => {
    return val instanceof Array
  },
  isEmpty: (val) => {
    return helpers.isBlank(val) || val.length <= 0 || Object.keys(val).length <= 0
  },
  isJSON: function(str) {
    try {
      JSON.parse(str)
    } catch (e) {
      return false
    }

    return true
  },
  toRaw(obj) {
    return JSON.parse(JSON.stringify(obj))
  },
  randomNumbers(from, to, length) {
    const numbers = [0]
    for (let i = 1; i < length; i++) {
      numbers.push(Math.ceil(Math.random() * (from - to) + to))
    }

    return numbers
  },
  getFullName(user, showMiddleName = false) {
    let fullName = ''

    if (user && user.FirstName) {
      fullName += user.FirstName
    }

    if (showMiddleName && user && user.MiddleName) {
      fullName += ` ${user.MiddleName}`
    }

    if (user && user.LastName) {
      fullName += ` ${user.LastName}`
    }

    return fullName
  },
  getFullAddress(item) {
    let Address = ''
    if (item && item.Street) {
      Address += item.Street + ', '
    }

    if (item && item.Barangay) {
      Address += (item.Barangay.toLowerCase().indexOf('barangay') > -1 ? '' : 'Barangay ') + item.Barangay + ', '
    }

    if (item && item.City) {
      Address += item.City + (item.Barangay.toLowerCase().indexOf('city') > -1 ? '' : ' City') + ', '
    }

    if (item && item.Province) {
      Address += item.Province + ', '
    }

    if (item && item.Country) {
      Address += item.Country + ', '
    }

    if (item && item.ZipCode) {
      Address += item.ZipCode
    }

    return Address
  },
  // getImage(item, type, placeholder) {
  //   let imageLink = ''
  //   if (helpers.isString(item)) {
  //     imageLink = configs.baseURL + `/image?name=${item}&type=${type}`
  //   } else if (item && helpers.isObject(item) && item.Image) {
  //     imageLink = configs.baseURL + `/image?name=${item.Image}&type=${type}`
  //   } else if (item && helpers.isObject(item) && item.Name) {
  //     imageLink = configs.baseURL + `/image?name=${item.Name}&type=${type}`
  //   } else {
  //     return placeholder && placeholder !== '' ? placeholder : require('@/assets/images/placeholder.jpg')
  //   }

  //   return imageLink
  // },
  openImage(imageLink) {
    const imageList = [imageLink]
    this.openImageList(imageList)
  },
  openImageList(imageList) {
    const photoBrowser = f7.photoBrowser.create({
      photos: imageList,
      routableModals: false,
      popupCloseLinkText: '<i class="far fa-fw fa-times"></i>',
      type: 'popup',
      toolbar: imageList.length > 1
    })

    photoBrowser.open()

    photoBrowser.on('close', function() {
      photoBrowser.destroy()
    })
  },
  showLoader(text = 'Loading...') {
    // f7.dialog.preloader(text)
    f7.preloader.show()
  },
  hideLoader() {
    // f7.dialog.close()
    f7.preloader.hide()
  },
  createNotification({ type, title = 'Notification', message = 'Click me to close', time = 'now' }) {
    let notificationCreated = false
    if (!notificationCreated) {
      const notification = f7.notification.create({
        title: configs.title,
        titleRightText: time,
        subtitle: `<strong class="${type}">${title}</strong>`,
        text: message,
        closeTimeout: 3000,
        closeOnClick: true
      })

      notification.open()

      notification.on('open', function() {
        notificationCreated = true
      })

      notification.on('close', function() {
        notification.destroy()
        notificationCreated = false
      })
    }
  },
  createConfirmation({ message, title = 'Confirm', confirm, cancel }) {
    const dialog = f7.dialog.confirm(message, title, confirm, cancel)
  },
  createRemarks({ message, title = 'Confirm', confirm, cancel }) {
    const dialog = f7.dialog.prompt(message, title, confirm, cancel)
  },
  catchError(err, showNotifications = false) {
    if (err.Message || err.Code) {
      if (showNotifications) {
        helpers.createNotification({
          type: 'error',
          title: 'Error!',
          message: err.Message || err.Code
        })
      }
    } else if (err && err.response && err.response.data && (err.response.data.Message || err.response.data.Code)) {
      if (showNotifications) {
        helpers.createNotification({
          type: 'error',
          title: 'Error!',
          message: err.response.data.Message || err.response.data.Code
        })
      }
    } else if (err && err.message) {
      if (showNotifications) {
        helpers.createNotification({
          type: 'error',
          title: 'Error!',
          message: err.message
        })
      }
    } else {
      if (showNotifications) {
        helpers.createNotification({
          type: 'error',
          title: 'Error!',
          message: 'Server not reachable, Please try again later.'
        })
      }
    }
  }
}

const install = (app) => {
  app.config.globalProperties.$h = helpers
}

export { install as default, helpers }
