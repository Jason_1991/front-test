import { required, minLength, maxLength } from '@vuelidate/validators'

const validations = {
  Username: {
    required,
    minLength: minLength(4),
    maxLength: maxLength(25)
  },
  Password: {
    required,
    minLength: minLength(6),
    maxLength: maxLength(25)
  }
}

export default validations
