import { library } from '@fortawesome/fontawesome-svg-core'

// DECLARATION FOR SOLID ICONS
// import {} from '@fortawesome/pro-solid-svg-icons'

// DECLARATION FOR REGULAR ICONS
import {
  faChevronDown,
  faChevronRight,
  faShoppingCart,
  faSearch,
  faPlus,
  faMinus
} from '@fortawesome/pro-regular-svg-icons'

// DECLARATION FOR LIGHT ICONS
// import {} from '@fortawesome/pro-light-svg-icons'

import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

// // INCLUDE TO LIBRARY FOR SOLID ICONS
// library.add()

// INCLUDE TO LIBRARY FOR REGULAR ICONS
library.add(faChevronDown, faChevronRight, faShoppingCart, faSearch, faPlus, faMinus)

// // INCLUDE TO LIBRARY FOR LIGHT ICONS
// library.add()

export { FontAwesomeIcon }
